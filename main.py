import os
import torch as tr
import torch.optim as optim
from argparse import ArgumentParser
from functools import partial
from neural_wrappers.utilities import changeDirectory, getGenerators
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics

from neural_wrappers.readers.carla_h5_reader import CarlaH5Reader as Reader, depthRenorm, rgbRenorm
from test_dataset import test_dataset
from test import test
from models import Model6DoF, Model6DoFSliceV1, ModelTinySum, ModelTinySumV2, Model6DoFSliceV2
from loss_functions import *

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataset_path")

	# Models
	parser.add_argument("--weights_file")

	# Reader and training stuff
	parser.add_argument("--model")
	parser.add_argument("--batch_size", default=5, type=int)
	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--dir")
	parser.add_argument("--lr", default=0.1, type=float)
	parser.add_argument("--num_neighbours", default=2, type=int)
	parser.add_argument("--pose_representation", default="6dof")
	parser.add_argument("--output_representation", default="rgbd")
	parser.add_argument("--resolution", default="512,512")
	parser.add_argument("--num_depth_slices", type=int, default=0)
	parser.add_argument("--slices_classification_loss", default="dice")
	parser.add_argument("--tanh_activation", type=int, default=1)
	parser.add_argument("--rgb_normalization", default="min_max_-1_1")
	parser.add_argument("--depth_normalization", default="min_max_-1_1")
	parser.add_argument("--pose_normalization", default="min_max_-1_1")
	parser.add_argument("--training_procedure", default="standard")

	args = parser.parse_args()
	args.resolution = tuple(map(lambda x : int(x), args.resolution.split(",")))
	assert args.type in ("train", "retrain", "test", "test_dataset", "live_navigation")
	assert args.pose_representation in ("6dof", "6dof-quat", "dot_distance_transform", "dot")
	assert args.output_representation in ("rgbd", "rgb", "depth")

	validModels = {
		"6dof" : ["Model6DoF", "Model6DoFSliceV1", "Model6DoFSliceV2"],
		"6dof-quat" : ["Model6DoF", "Model6DoFSliceV1", "Model6DoFSliceV2"],
		"dot_distance_transform" : ["ModelTinySum", "ModelTinySumV2"],
		"dot" : ["ModelTinySum", "ModelTinySumV2"]
	}[args.pose_representation]
	assert args.model in validModels

	if args.model in ["Model6DoFSliceV1", "Model6DoFSliceV2", "ModelTinySumV2"]:
		assert args.num_depth_slices > 1, "Slice models must define a number of slices"
	if args.num_depth_slices > 1:
		assert args.output_representation in ("rgbd", "depth")
		assert args.slices_classification_loss in ("bce", "dice")
	args.tanh_activation = bool(args.tanh_activation)
	assert args.rgb_normalization in ("min_max_-1_1", "min_max_0_1")
	assert args.depth_normalization in ("min_max_-1_1", "min_max_0_1")
	assert args.pose_normalization in ("min_max_-1_1", "min_max_0_1")
	assert args.training_procedure in ("standard", "adversarial")
	# Some constraints, not all are necessary probably.
	if args.training_procedure == "adversarial":
		assert args.tanh_activation == True
		assert args.pose_representation in ("6dof", "6dof-quat")
		assert args.depth_normalization == "min_max_-1_1"
		assert args.rgb_normalization == "min_max_-1_1"

	return args

def getLossesAndMetrics(args):
	fDepthRenorm = partial(depthRenorm, depthNormalization=args.depth_normalization, depthStats=args.depthStats)
	fRgbRenorm = partial(rgbRenorm, rgbNormalization=args.rgb_normalization)
	fDepthMetric = partial(depthMetric, depthRenorm=fDepthRenorm)
	fRgbMetricL1Pixel = partial(RGBMetricL1Pixel, rgbRenorm=fRgbRenorm)

	metrics = {
		"rgbd" : {"Depth (m)" : fDepthMetric, "RGB (L1 pixel)" : fRgbMetricL1Pixel, "RGB (L2)" : RGBMetricL2},
		"rgb" : {"RGB (L1 pixel)" : fRgbMetricL1Pixel, "RGB (L2)" : RGBMetricL2},
		"depth" : {"Depth (m)" : fDepthMetric}
	}[args.output_representation]
	lossSlices = None
	if args.num_depth_slices > 1:
		if args.slices_classification_loss == "dice":
			metrics["Dice Coefficient"] = diceMetric
			lossSlices = diceLoss
		elif args.slices_classification_loss == "bce":
			metrics["Binary Cross Entropy"] = bceMetric
			lossSlices = bceLoss

	lossFunctionsWeights = {}
	if args.output_representation in ("rgbd", "depth"):
		lossFunctionsWeights["depth"] = (1, lossDepth)
	if args.output_representation in ("rgbd", "rgb"):
		lossFunctionsWeights["rgb"] = (1, lossRgb)
	if not lossSlices is None:
		lossFunctionsWeights["depthSlices"] = (1, lossSlices)

	return lossFunctionsWeights, metrics

def getModel(args):
	dOut = {
		"rgbd" : 4,
		"rgb" : 3,
		"depth" : 1
	}[args.output_representation]

	dIn = {
		"6dof" : 6,
		"6dof-quat" : 7,
		"dot_distance_transform" : 9,
		"dot" : 9
	}[args.pose_representation]

	hyperParameters = {
		"resolution" : args.resolution, "numNeighbours" : args.num_neighbours,
		"poseRepresentation" : args.pose_representation, "outputRepresentation" : args.output_representation,
		"TanhActivation" : args.tanh_activation, "PoseNormalization" : args.pose_normalization
	}
	if args.output_representation in ("rgbd", "depth"):
		hyperParameters["DepthNormalization"] = args.depth_normalization
	if args.output_representation in ("rgbd", "rgb"):
		hyperParameters["RGBNormalization"] = args.rgb_normalization
	if args.num_depth_slices > 1:
		hyperParameters["numDepthSlices"] = args.num_depth_slices
		hyperParameters["DepthSlicesClassificationLoss"] = args.slices_classification_loss

	fActivation = (lambda x : x) if not args.tanh_activation else tr.tanh
	outputFunc = {
		"rgbd" : lambda x : {"rgb" : fActivation(x[..., 0 : 3]), "depth" : fActivation(x[..., 3])},
		"rgb" : lambda x : {"rgb" : fActivation(x[..., 0 : 3])},
		"depth" : lambda x : {"depth" : fActivation(x[..., 0])}
	}[args.output_representation]

	model = {
		"Model6DoF" : partial(Model6DoF, dIn=dIn, dOut=dOut, outputFunc=outputFunc),
		"Model6DoFSliceV1" : partial(Model6DoFSliceV1, dIn=dIn, dOut=dOut, outputFunc=outputFunc),
		"Model6DoFSliceV2" : partial(Model6DoFSliceV2, dIn=dIn, dOut=dOut, outputFunc=outputFunc),
		"ModelTinySum" : partial(ModelTinySum, dIn=dIn, dOut=dOut, outputFunc=outputFunc),
		"ModelTinySumV2" : partial(ModelTinySumV2, dIn=dIn, dOut=dOut, outputFunc=outputFunc)
	}[args.model](hyperParameters=hyperParameters).to(device)

	lossFunctionsWeights, metrics = getLossesAndMetrics(args)
	plotMetrics = {
		"rgbd" : ["Loss", "Depth (m)", "RGB (L1 pixel)"],
		"rgb" : ["Loss", "RGB (L1 pixel)"],
		"depth" : ["Loss", "Depth (m)"],
	}[args.output_representation]

	callbacks = [SaveHistory("history.txt"), SaveModels("best"), PlotMetrics(plotMetrics)]
	if args.output_representation in ("rgbd", "depth"):
		callbacks.append(SaveModels("best", metric="Depth (m)"))
	if args.output_representation in ("rgbd", "rgb"):
		callbacks.append(SaveModels("best", metric="RGB (L1 pixel)"))

	model.addMetrics(metrics)
	model.setOptimizer(optim.AdamW, lr=args.lr)
	model.addCallbacks(callbacks)

	if args.training_procedure == "adversarial":
		assert False, "TODO"
		from models import ModelAdversarial
		model = ModelAdversarial(generator=model)
		model.discriminator.setOptimizer(optim.AdamW, lr=args.lr)
		# Add a dummy criterion for now, so we just train adversarially.
	elif args.training_procedure == "standard":
		model.setCriterion(partial(lossFn, lossFunctionsWeights=lossFunctionsWeights))

	return model

def main():
	args = getArgs()
	reader = Reader(args.dataset_path, poseRepresentation=args.pose_representation, \
		numNeighbours=args.num_neighbours, numDepthSlices=args.num_depth_slices, \
		normalization = {"rgb" : args.rgb_normalization, "depth" : args.depth_normalization, \
			"position" : args.pose_normalization}, \
		resizer = {"rgb" : (*args.resolution, 3), "depth" : args.resolution})
	args.depthStats = reader.depthStats
	generator, numSteps, valGenerator, valNumSteps = getGenerators(reader, args.batch_size, maxPrefetch=0)

	model = getModel(args)
	print(model.summary())

	if args.type == "train":
		changeDirectory(args.dir, expectExist=False)
		model.train_generator(generator, numSteps, args.num_epochs, valGenerator, valNumSteps)
	elif args.type == "retrain":
		model.loadModel(args.weights_file)
		changeDirectory(args.dir, expectExist=True)
		model.train_generator(generator, numSteps, args.num_epochs, valGenerator, valNumSteps)
	elif args.type == "test":
		model.loadModel(args.weights_file)
		weightsDir = "/".join(os.path.abspath(args.weights_file).split("/")[0:-1])
		fRgbRenorm = partial(rgbRenorm, rgbNormalization=args.rgb_normalization)
		fDepthRenorm = partial(depthRenorm, depthNormalization=args.depth_normalization, depthStats=args.depthStats)
		test(model, valGenerator, valNumSteps, weightsDir, fRgbRenorm, fDepthRenorm)
	elif args.type == "test_dataset":
		fRgbRenorm = partial(rgbRenorm, rgbNormalization=args.rgb_normalization)
		fDepthRenorm = partial(depthRenorm, depthNormalization=args.depth_normalization, depthStats=args.depthStats)
		test_dataset(generator, fRgbRenorm, fDepthRenorm)
	elif args.type == "live_navigation":
		from live_navigation import live_navigation
		model.loadModel(args.weights_file)
		model.hyperParameters["extremes"] = {"positions" : reader.positionsExtremes, \
			"rotation" : reader.rotationExtremes}
		live_navigation(model)

if __name__ == "__main__":
	main()