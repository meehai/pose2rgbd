import sys
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import transforms3d.euler as txe
from sklearn.preprocessing import normalize

from PyQt5.QtGui import QImage, QPalette, QPixmap
from PyQt5.QtWidgets import QAction, QApplication, QLabel, QMainWindow, QMenu, QScrollArea, QSizePolicy, QShortcut
from PyQt5.QtGui import QKeySequence

# Based on: https://learnopengl.com/Getting-started/Camera
class Camera:
    def __init__(self, modelHyperParameters):
        self.cameraPos = np.array([0.4, 0.3, 0], dtype=np.float32)
        self.cameraTarget = np.array([0.2, 0.1, 0], dtype=np.float32)
        self.speed = 0.01
        self.modelHyperParameters = modelHyperParameters

        self.inputMask = self.getInputMask(modelHyperParameters["extremes"])
        self.setup()

    # Some models have some of the 6DoF fixed (i.e. altitude always 0 or some orientation angle always fixed). We make
    #  a custom mask so we always set those inputs to 0 to not leave the training space.
    def getInputMask(self, extremes):
        transMask = extremes["positions"]["min"] == extremes["positions"]["max"]
        rotMask = extremes["rotation"]["min"] == extremes["rotation"]["max"]
        return ~np.concatenate([transMask, rotMask])

    def norm(x):
        return normalize([x])[0]

    def setup(self):
        self.cameraUp = np.array([0, 1, 0])
        self.cameraPos = np.clip(self.cameraPos, 0, 1)
        self.cameraFront = Camera.norm(self.cameraTarget - self.cameraPos)

    def forward(self):
        self.setup()
        self.cameraPos += self.speed * self.cameraFront
        self.cameraPos = np.clip(self.cameraPos, 0, 1)

    def backward(self):
        self.setup()
        self.cameraPos -= self.speed * self.cameraFront
        self.cameraPos = np.clip(self.cameraPos, 0, 1)

    def get6DoF(self):
        # cameraPos is [0 : 1], move it to [-1 : 1] and apply mask
        self.setup()
        cat6DoF = np.concatenate([(self.cameraPos - 0.5) * 2, self.cameraFront]).astype(np.float32)
        cat6DoF = cat6DoF * self.inputMask
        return cat6DoF

    def getInput(self):
        input6DoF = self.get6DoF()

        f6DoFToNormed = {
            "min_max_-1_1" : lambda x : x,
            "min_max_0_1" : lambda x : (x / 2) + 0.5
        }[self.modelHyperParameters["PoseNormalization"]]

        fNormedToInput = {
            "6dof" : lambda x : x,
            "6dof-quat" : lambda x : np.concatenate([x[0 : 3], txe.euler2quat(*x[3 : ])])
        }[self.modelHyperParameters["poseRepresentation"]]

        a = input6DoF
        b = f6DoFToNormed(a)
        c = fNormedToInput(b)
        print("Input:", c, "6DoF:", a)
        return np.expand_dims(c, axis=0).astype(np.float32)

class ImageViewer(QMainWindow):
    def __init__(self, model):
        super(ImageViewer, self).__init__()

        self.imageLabel = QLabel()
        self.imageLabel.setBackgroundRole(QPalette.Base)
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)

        self.scrollArea = QScrollArea()
        self.scrollArea.setBackgroundRole(QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)
        self.scrollArea.setWidgetResizable(True)
        self.setCentralWidget(self.scrollArea)

        self.createActions()
        self.createMenus()
        self.createShortcuts()

        self.setWindowTitle("Pose2RGBD Viewer")
        self.resize(1000, 500)

        self.model = model
        self.colorMap = plt.get_cmap("plasma")
        self.camera = Camera(self.model.hyperParameters)

    def open(self):
        image = Image.open("results/new/6dof_quat/depth_10slice_v2/2.png")
        npImage = np.array(image, dtype=np.uint8)[..., 0 : 3].copy()
        self.putImage(npImage)

    def reset(self):
        self.camera = Camera(self.model.hyperParameters)
        self.render()

    def render(self):
        currentPosition = self.camera.getInput()
        x = {"position" : currentPosition}
        res = self.model.npForward(x)
        # print(x["position"], x["position"].min(), x["position"].max(), res["rgb"].min(), res["rgb"].max(), res["depth"].min(), res["depth"].max())
        pngImage = (res["rgb"][0] + 1) / 2 * 255
        dphImage = (res["depth"][0] + 1) / 2
        dphPlasma = self.colorMap(dphImage)[..., 0 : 3] * 255

        combined = np.hstack([pngImage, dphPlasma]).astype(np.uint8)
        self.putImage(combined)

    def putImage(self, image):
        qImage = QImage(image, image.shape[1], image.shape[0], QImage.Format_RGB888)
        self.imageLabel.setPixmap(QPixmap.fromImage(qImage))

    def createActions(self):
        self.openAct = QAction("&Reset", self, shortcut="Ctrl+R", triggered=self.reset)
        self.exitAct = QAction("E&xit", self, shortcut="Ctrl+Q", triggered=lambda _ : exit())

    def createMenus(self):
        self.fileMenu = QMenu("&File", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)
        self.menuBar().addMenu(self.fileMenu)

    def createShortcuts(self):
        QShortcut(QKeySequence("W"), self).activated.connect(lambda: (self.camera.forward(), self.render()))
        # QShortcut(QKeySequence("A"), self).activated.connect(lambda: (self.moveForward(), self.render()))
        QShortcut(QKeySequence("S"), self).activated.connect(lambda: (self.camera.backward(), self.render()))
        # QShortcut(QKeySequence("D"), self).activated.connect(lambda: (self.moveForward(), self.render()))

def live_navigation(model):
    app = QApplication([])
    imageViewer = ImageViewer(model)
    imageViewer.show()
    sys.exit(app.exec_())
